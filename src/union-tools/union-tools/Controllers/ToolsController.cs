﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using DocumentGenerator;
using DocumentGenerator.Model;
using GoogleDocsService;
using GoogleDocsService.Model;
using union_tools.Helpers;

namespace union_tools.Controllers
{

    public class ToolsController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="number"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/message/send")]
        public HttpResponseMessage SendMessage(string number, string message)
        {
            var formPoster = new FormPostHelper();
            var response = formPoster.Post("http://smsgateway.me/api/v3/messages/send", new NameValueCollection() {
                { "email", "iyousifi@gmail.com" },
                { "password", "Infin810" },
                { "device", "9048"},
                {"number" , "+45" + number},
                {"message",  message}
            });
            string result = System.Text.Encoding.UTF8.GetString(response);
            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/invoice/test")]
        public async Task<string> Test()
        {
            //var invoiceGenerator = new HussainiaInvoiceGenerator();
            //var filename = invoiceGenerator.MakeInvoice(new Invoice
            //{
            //    Customer = new Customer
            //    {
            //        Name = "Muhammad Musa",
            //        StreetName = "Bellahøjvej",
            //        Title = "Mr. ",
            //        ZipCodeAndCity = ""
            //    },
            //    Items = new List<Item>() { new Item
            //    {
            //        Discount = 0, 
            //        Price = 250, 
            //        Quantity = 6,
            //        Text = "Kontingent Jul - Dec 2015"
            //    }
                
            //    },
            //    Total = 1000,
            //    Recipient = "Imran Yousifi",
            //    PaymentMethod = "Kontant"
            //});
            //var mailer = new MailHelper();
            //var mailSuccess = mailer.SendMail("hussainia.imam.mahdi@gmail.com", "iyousifi@gmail.com",
            //    "Kvittering for kontingent", "Hermed din kvittering", filename);
            //return mailSuccess;
            
            var test = new SpreadSheetService("test");
            await test.InsertSpreadSheetLine(new PaymentCommand(), HostingEnvironment.MapPath("~/App_Data/google-service-account.p12"));
            throw new NotImplementedException();
        }


        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/invoice/")]
        public void CreateInvoice(Invoice invoice)
        {
            
        }
    }
}
