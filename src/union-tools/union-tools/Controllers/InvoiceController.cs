﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DocumentGenerator;
using DocumentGenerator.Model;
using GoogleDocsService;
using GoogleDocsService.Model;
using union_tools.Helpers;

namespace union_tools.Controllers
{
    public class InvoiceController : Controller
    {
        // GET: Invoice
        public ActionResult Index()
        {
            return View();
        }

        // GET: Invoice/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Invoice/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Invoice/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                var invoiceGenerator = new HussainiaInvoiceGenerator();
                int amount = 0;
                int.TryParse(collection["price"], out amount);
                if(amount == 0)
                    throw new HttpException();
                var invoice = new Invoice
                {
                    Customer = new Customer
                    {
                        Name = collection["name"],
                    },
                    Items = new List<Item>()
                    {
                        new Item
                        {
                            Discount = 0,
                            Price = amount,
                            Quantity = 1,
                            Text = collection["text"]
                        }

                    },
                    Total = amount,
                    Recipient = collection["recipient"],
                    PaymentMethod = collection["paymentmethod"]
                };
                var path = Server.MapPath("~/pdfs");
                var filename = invoiceGenerator.MakeInvoice(invoice, path);
                var mailer = new MailHelper();
                var mailSuccess = mailer.SendMail("hussainia.imam.mahdi@gmail.com", "hussainia.imam.mahdi@gmail.com", "Kvittering for kontingent", "Hermed din kvittering", filename);
                if(!string.IsNullOrEmpty(collection["email"]))
                mailer.SendMail("hussainia.imam.mahdi@gmail.com", collection["email"], "Kvittering for kontingent", "Hermed din kvittering", filename);

                //var googleRepo = new SpreadSheetService("AIzaSyD-ecaUvpIfeU4-P0N2xb9mSPNUWF6iDAY");
                //googleRepo.InsertSpreadSheetLine(new PaymentCommand
                //{
                //    Amount = amount.ToString(),
                //    DateTime = DateTime.Now,
                //    Name = invoice.Customer.Name,
                //    Text = invoice.Items.First().Text,
                //    Recipient = invoice.Recipient
                //}, "");


                return View();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Invoice/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Invoice/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Invoice/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Invoice/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
