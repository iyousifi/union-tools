﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace union_tools.Models
{
    public class InvoiceCommand
    {
        //Customer
        public string Title { get; set; }
        public string Name { get; set; }
        public string StreetName { get; set; }
        public string ZipCodeAndCity { get; set; }
        public string Email { get; set; }

        //Item
        public int Quantity { get; set; }
        public string Text { get; set; }
        public double Discount { get; set; }
        public double Price { get; set; }

        //Invoice
        public double Total { get; set; }
        public string Recipient { get; set; }
        public string PaymentMethod { get; set; }
    }
}