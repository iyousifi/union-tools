﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Web;

namespace union_tools.Helpers
{
    public class MailHelper
    {
        public string SendMail(string sender, string recipient, string subject, string body, string attachmentPath)
        {
            MailMessage msg = new MailMessage();

            msg.From = new MailAddress(sender);
            msg.To.Add(recipient);
            msg.Subject = subject;
            msg.Body = body;
            msg.Attachments.Add(new Attachment(attachmentPath));
            SmtpClient client = new SmtpClient();
            client.UseDefaultCredentials = false;
            client.Host = "smtp.gmail.com";
            client.Port = 587;
            client.EnableSsl = true;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.Credentials = new NetworkCredential("hussainia.imam.mahdi@gmail.com", "StrongLeadershipIsNeeded");
            client.Timeout = 20000;
            try
            {
                client.Send(msg);
                return "Mail has been successfully sent!";
            }
            catch (Exception ex)
            {
                return "Error sending email: " + ex.Message;
            }
            finally
            {
                msg.Dispose();
            }
        }
    }
}