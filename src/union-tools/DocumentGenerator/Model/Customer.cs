﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentGenerator.Model
{
    public class Customer
    {
        public string Title { get; set; }
        public string Name { get; set; }
        public string StreetName { get; set; }
        public string ZipCodeAndCity { get; set; }
    }
}
