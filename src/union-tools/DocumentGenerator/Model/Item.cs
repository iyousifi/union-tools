﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentGenerator.Model
{
    public class Item
    {
        public int Quantity { get; set; }
        public string Text { get; set; }
        public double Discount { get; set; }
        public double Price { get; set; }
    }
}
