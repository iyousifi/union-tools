﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentGenerator.Model
{
    public class Invoice
    {
        public Customer Customer { get; set; }
        public List<Item> Items { get; set; }
        public double Total { get; set; }
        public string Recipient { get; set; }
        public string PaymentMethod { get; set; }
    }
}
