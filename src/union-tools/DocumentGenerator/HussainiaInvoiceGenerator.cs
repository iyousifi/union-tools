﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.XPath;
using DocumentGenerator.Model;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;

namespace DocumentGenerator
{
    public class HussainiaInvoiceGenerator
    {
        public string MakeInvoice(Invoice invoice, string path)
        {
            // Create a MigraDoc document
            Document document = CreateDocument(invoice);
            
            //string ddl = MigraDoc.DocumentObjectModel.IO.DdlWriter.WriteToString(document);
            

            if (!System.IO.Directory.Exists(path))
            {
                System.IO.Directory.CreateDirectory(path);
            }
            MigraDoc.DocumentObjectModel.IO.DdlWriter.WriteToFile(document, path + "\\MigraDoc.mdddl");
            
            PdfDocumentRenderer renderer = new PdfDocumentRenderer(true, PdfSharp.Pdf.PdfFontEmbedding.Always);
            renderer.Document = document;
            
            renderer.RenderDocument();
            // Save the document...
            string filename = string.Format("Kvittering {0} {1}.pdf", invoice.Customer.Name, DateTime.Now.ToShortDateString());
            renderer.PdfDocument.Save(filename);
            // ...and start a viewer.
            //Process.Start(filename);
            return filename;
        }

        private Document document;
        private Table table;

        public Document CreateDocument(Invoice invoice)
        {
            // Create a new MigraDoc document
            this.document = new Document();
            this.document.Info.Title = "Kvittering for kontingent";
            this.document.Info.Author = "Hussainia Imam Mahdi(aj)";

            DefineStyles();
            CreatePage();
            FillContent(invoice);
            return this.document;
        }

        void DefineStyles()
        {
            // Get the predefined style Normal.
            Style style = this.document.Styles["Normal"];
            // Because all styles are derived from Normal, the next line changes the 
            // font of the whole document. Or, more exactly, it changes the font of
            // all styles and paragraphs that do not redefine the font.
            style.Font.Name = "Verdana";

            style = this.document.Styles[StyleNames.Header];
            style.ParagraphFormat.AddTabStop("16cm", TabAlignment.Right);
            style = this.document.Styles[StyleNames.Footer];
            style.ParagraphFormat.AddTabStop("8cm", TabAlignment.Center);
            // Create a new style called Table based on style Normal
            style = this.document.Styles.AddStyle("Table", "Normal");
            style.Font.Name = "Verdana";
            style.Font.Name = "Arial";
            style.Font.Size = 9;
            // Create a new style called Reference based on style Normal
            style = this.document.Styles.AddStyle("Reference", "Normal");
            style.ParagraphFormat.SpaceBefore = "5mm";
            style.ParagraphFormat.SpaceAfter = "5mm";
            style.ParagraphFormat.TabStops.AddTabStop("16cm", TabAlignment.Right);
        }
        void CreatePage()
        {
            // Each MigraDoc document needs at least one section.
            Section section = this.document.AddSection();
            
            // Put a logo in the header
            Image image = section.Headers.Primary.AddImage("~\\images\\Hussaina-logo-small.png");
            image.Height = "2.5cm";
            image.LockAspectRatio = true;
            image.RelativeVertical = RelativeVertical.Line;
            image.RelativeHorizontal = RelativeHorizontal.Margin;
            image.Top = ShapePosition.Top;
            image.Left = ShapePosition.Right;
            image.WrapFormat.Style = WrapStyle.Through;
            // Create footer
            Paragraph paragraph = section.Footers.Primary.AddParagraph();
            paragraph.AddText("Hussainia Imam Mahdi(aj) · Trekronergade 149 E · 2500 Valby");
            paragraph.Format.Font.Size = 9;
            paragraph.Format.Alignment = ParagraphAlignment.Center;
            // Create the text frame for the address
            this.addressFrame = section.AddTextFrame();
            this.addressFrame.Height = "3.0cm";
            this.addressFrame.Width = "7.0cm";
            this.addressFrame.Left = ShapePosition.Left;
            this.addressFrame.RelativeHorizontal = RelativeHorizontal.Margin;
            this.addressFrame.Top = "5.0cm";
            this.addressFrame.RelativeVertical = RelativeVertical.Page;
            // Put sender in address frame
            paragraph = this.addressFrame.AddParagraph("Hussainia Imam Mahdi(aj) · Trekronergade 149 E · 2500 Valby");
            paragraph.Format.Font.Name = "Arial";
            paragraph.Format.Font.Size = 7;
            paragraph.Format.SpaceAfter = 3;
            // Add the print date field
            paragraph = section.AddParagraph();
            paragraph.Format.SpaceBefore = "8cm";
            paragraph.Style = "Reference";
            paragraph.AddFormattedText("KVITTERING FOR KONTINGENT", TextFormat.Bold);
            paragraph.AddTab();
            paragraph.AddText("Valby ");
            paragraph.AddDateField("d. MMMM yyyy");
            // Create the item table
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Borders.Color = new Color(0,0,0,0);
            this.table.Borders.Width = 0.25;
            this.table.Borders.Left.Width = 0.5;
            this.table.Borders.Right.Width = 0.5;
            this.table.Rows.LeftIndent = 0;
            
            // Before you can add a row, you must define the columns
            //Column column = this.table.AddColumn("1cm");
            //column.Format.Alignment = ParagraphAlignment.Center;
            
            //column = this.table.AddColumn("2.5cm");
            //column.Format.Alignment = ParagraphAlignment.Right;
            
            //column = this.table.AddColumn("3cm");
            //column.Format.Alignment = ParagraphAlignment.Right;
            
            Column column = this.table.AddColumn("8cm");
            column.Format.Alignment = ParagraphAlignment.Right;

            column = this.table.AddColumn("2cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("2cm");
            column.Format.Alignment = ParagraphAlignment.Center;
            
            column = this.table.AddColumn("4cm");
            column.Format.Alignment = ParagraphAlignment.Right;
            
            // Create the header of the table
            Row row = table.AddRow();
            row.Height = "1cm";
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = true;
            row.Shading.Color = new Color(0,0,0,0);
            //row.Cells[0].AddParagraph("No.");
            //row.Cells[0].Format.Font.Bold = false;
            //row.Cells[0].Format.Alignment = ParagraphAlignment.Left;
            //row.Cells[0].VerticalAlignment = VerticalAlignment.Bottom;
            //row.Cells[0].MergeDown = 1;
            row.Cells[0].AddParagraph("Beskrivelse");
            row.Cells[0].Format.Alignment = ParagraphAlignment.Left;
            //row.Cells[0].MergeRight = 3;
            row.Cells[1].AddParagraph("Antal mdr.");
            row.Cells[1].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[2].AddParagraph("Pris");
            row.Cells[2].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[3].AddParagraph("Total");
            row.Cells[3].Format.Alignment = ParagraphAlignment.Left;
            //row = table.AddRow();
            //row.HeadingFormat = true;
            //row.Format.Alignment = ParagraphAlignment.Center;
            //row.Format.Font.Bold = true;
            //row.Shading.Color = new Color(0, 0, 0, 0);
            
            this.table.SetEdge(0, 0, 4, 1, Edge.Box, BorderStyle.Single, 0.75, Color.Empty);
        }
        public void FillContent(Invoice invoice)
        {
            // Fill address in address text frame
            Paragraph paragraph = this.addressFrame.AddParagraph();
            paragraph.AddText(invoice.Customer.Name); //Name
            
            if (!string.IsNullOrEmpty(invoice.Customer.StreetName))
            {
                paragraph.AddLineBreak();
                paragraph.AddText(invoice.Customer.StreetName); //StreetName
            }
            if (!string.IsNullOrEmpty(invoice.Customer.ZipCodeAndCity))
            {
                paragraph.AddLineBreak();
                paragraph.AddText(invoice.Customer.ZipCodeAndCity);
            }

            // Iterate the invoice items
            double totalExtendedPrice = 0;
            var TableGray = new Color(0,50,50,50);
            foreach (var item in invoice.Items)
            {
      
                double quantity = item.Quantity;
                double price = item.Price;
                double discount = item.Discount;
                
                // Each item fills two rows
                Row row1 = this.table.AddRow();
                row1.Height = "1cm";
                row1.TopPadding = 1.5;
                row1.Cells[0].Shading.Color = TableGray;
                row1.Cells[0].Format.Alignment = ParagraphAlignment.Left;
                row1.Cells[1].Format.Alignment = ParagraphAlignment.Right;
                row1.Cells[2].Shading.Color = TableGray;
                row1.Cells[2].Format.Alignment = ParagraphAlignment.Right;
                
                paragraph = row1.Cells[0].AddParagraph();
                paragraph.AddFormattedText(item.Text, TextFormat.Bold);
                row1.Cells[1].AddParagraph(quantity.ToString());
                row1.Cells[2].AddParagraph( price.ToString("0.00"));
                double extendedPrice = quantity * price;
                extendedPrice = extendedPrice * (100 - discount) / 100;
                row1.Cells[3].AddParagraph(extendedPrice.ToString("0.00"));
                totalExtendedPrice += extendedPrice;
                
                this.table.SetEdge(0, this.table.Rows.Count - 2, 4, 1, Edge.Box, BorderStyle.Single, 0.75);
            }
            
            // Add an invisible row as a space line to the table
            Row row = this.table.AddRow();
            row.Borders.Visible = false;
            
            // Add the total price row
            row = this.table.AddRow();
            row.Cells[0].Borders.Visible = false;
            row.Cells[0].AddParagraph("I alt ");
            row.Cells[0].Format.Font.Bold = true;
            row.Cells[0].Format.Alignment = ParagraphAlignment.Right;
            row.Cells[0].MergeRight = 2;
            row.Cells[3].AddParagraph(totalExtendedPrice.ToString("0.00"));
            
           
            //// Add the total due row
            //row = this.table.AddRow();
            //row.Cells[0].AddParagraph("Total Due");
            //row.Cells[0].Borders.Visible = false;
            //row.Cells[0].Format.Font.Bold = true;
            //row.Cells[0].Format.Alignment = ParagraphAlignment.Right;
            //row.Cells[0].MergeRight = 4;
            //totalExtendedPrice += 0.19 * totalExtendedPrice;
            //row.Cells[5].AddParagraph(totalExtendedPrice.ToString("0.00") + " €");
            
            //// Set the borders of the specified cell range
            //this.table.SetEdge(5, this.table.Rows.Count - 4, 1, 4, Edge.Box, BorderStyle.Single, 0.75);
            
            // Add the notes paragraph
            paragraph = this.document.LastSection.AddParagraph();
            paragraph.Format.SpaceBefore = "1cm";
            paragraph.Format.Borders.Width = 0.75;
            paragraph.Format.Borders.Distance = 3;
            paragraph.Format.Borders.Color = new Color(0, 0, 0, 0);
            paragraph.Format.Shading.Color = new Color(0, 0, 0, 100);
            paragraph.AddText(string.Format("Modtaget af {0}. Betalingsmetode: {1}", invoice.Recipient, invoice.PaymentMethod));
        }


        public TextFrame addressFrame { get; set; }
    }
}
