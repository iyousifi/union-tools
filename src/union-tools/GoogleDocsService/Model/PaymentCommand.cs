﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleDocsService.Model
{
    public class PaymentCommand
    {
        public string Name { get; set; }
        public string Text { get; set; }
        public string Amount { get; set; }
        public DateTime DateTime { get; set; }
        public string Recipient { get; set; }
    }
}
