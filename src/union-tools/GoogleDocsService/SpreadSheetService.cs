﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Channels;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using Google.GData.Client;
using Google.GData.Spreadsheets;
using GoogleDocsService.Model;

namespace GoogleDocsService
{
    public class SpreadSheetService
    {
        private readonly SpreadsheetsService _sheetService;

        public SpreadSheetService(string token)
        {
            _sheetService = new SpreadsheetsService("test");
            _sheetService.SetAuthenticationToken(token);
        }

        public async Task<WorksheetEntry> GetWorksheet(string spreadSheetName, string filepath)
        {
            var query = new SpreadsheetQuery();
            var service = await ConnectToServiceAccount(filepath, "774658095423-mm4ms3kpbdjr8brgjghaki4m3chs1bn9@developer.gserviceaccount.com");
            var feed = service.Query(query);


            var spreadSheetEntry =
                (SpreadsheetEntry)feed.Entries.FirstOrDefault(s => s.Title.Text.Contains(spreadSheetName));

            if (spreadSheetEntry == null)
                throw new EntryPointNotFoundException("spreadsheet not found");

            return (WorksheetEntry)spreadSheetEntry.Worksheets.Entries.First();
        }
        private async Task<SpreadsheetsService> ConnectToServiceAccount(string keyFile, string accountEmail)
        {
            //var secrets = GoogleClientSecrets.Load(stream).Secrets;
            var certificate = new X509Certificate2(keyFile, "notasecret", X509KeyStorageFlags.Exportable);

            var initializer = new ServiceAccountCredential.Initializer(accountEmail)
            {
                Scopes = new[] { "https://spreadsheets.google.com/feeds", "https://spreadsheets.google.com/feeds/spreadsheets/private/full" }
            }.FromCertificate(certificate);

            var credential = new ServiceAccountCredential(initializer);

            await credential.RequestAccessTokenAsync(CancellationToken.None);
            var requestFactory = new Google.GData.Client.GDataRequestFactory("Hussainia Accounting Service");
            requestFactory.CustomHeaders.Add(string.Format("Authorization: Bearer {0}", credential.Token.AccessToken));

            // Create the service.
            var service = new SpreadsheetsService("whatever");

            service.RequestFactory = requestFactory;
            return service;

        }
        //public IEnumerable<string> GetPhoneNumbers()
        //{
        //    var worksheet = GetWorksheet("HussainiaTelefonliste", "iyousifi@gmail.com", "Pegasus820");

        //    AtomLink listFeedLink = worksheet.Links.FindService(GDataSpreadsheetsNameTable.ListRel, null);

        //    // Fetch the list feed of the worksheet.
        //    var listQuery = new ListQuery(listFeedLink.HRef.ToString());
        //    var listFeed = _sheetService.Query(listQuery);

        //    // Iterate through each row, printing its cell values.
        //    return listFeed.Entries.Cast<ListEntry>().Select(row => row.Elements[3].Value).Where(value => !string.IsNullOrEmpty(value));
        //}

        public async Task InsertSpreadSheetLine(PaymentCommand paymentCommand, string path)
        {
            var worksheet = await GetWorksheet("test", path);
            // Define the URL to request the list feed of the worksheet.
            AtomLink listFeedLink = worksheet.Links.FindService(GDataSpreadsheetsNameTable.ListRel, null);

            // Fetch the list feed of the worksheet.
            ListQuery listQuery = new ListQuery(listFeedLink.HRef.ToString());
            ListFeed listFeed = _sheetService.Query(listQuery);

            // Create a local representation of the new row.
            ListEntry row = new ListEntry();
            row.Elements.Add(new ListEntry.Custom() { LocalName = "name", Value = paymentCommand.Name });
            row.Elements.Add(new ListEntry.Custom() { LocalName = "text", Value = paymentCommand.Text });
            row.Elements.Add(new ListEntry.Custom() { LocalName = "amount", Value = paymentCommand.Amount });
            row.Elements.Add(new ListEntry.Custom() { LocalName = "recipient", Value = paymentCommand.Recipient });
            row.Elements.Add(new ListEntry.Custom() { LocalName = "Datereceived", Value = paymentCommand.DateTime.ToShortDateString() });

            // Send the new row to the API for insertion.
            _sheetService.Insert(listFeed, row);
        }
    }
}
